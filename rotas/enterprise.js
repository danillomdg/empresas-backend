const {db} = require('../database');
const router = require('express').Router();
const verify = require('./verifyToken');
const json_empresas = require('../empresas_json');

//Lista empresas
router.get('/',verify, (req, res) => {
    let sql = 'SELECT * FROM enterprise'
    //define os filtros
    let enterprise_types = req.query.enterprise_types;
    let name = req.query.name;
    //testa os filtros
    let where = ""
    let and = ""
    let filterEmpresas = ""
    let filterName = ""
    if (enterprise_types || name) where = " WHERE"  
    if (enterprise_types && name) and = " AND"
    if (enterprise_types) filterEmpresas = ` enterprise_type_code = '${enterprise_types}'`
    if (name) filterName = ` enterprise_name LIKE '%${name}%'`

    //Cria a sintaxe sql e realiza a query
    let sql_final = sql.concat(where,filterEmpresas,and,filterName)
    console.log(sql_final)
    if (db.state != 'disconnected') 
        {
        let query = db.query(sql_final, (err, results) => {
            if (err) throw err;
            res.send({"enterprises": results});
            //res.send(res.statusCode)
        })
    }
    else //Em caso de conexao com host externo
    {
        try{
            res.send(res.body);
            }catch(err){  res.status(500).send('Internal server error') }
    }
})


//detalha empresa
router.get('/:id', verify, (req, res) => {
    let sql = `SELECT * FROM enterprise WHERE id = ${req.params.id}`
    if (db.state != 'disconnected') 
    { 
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            console.log(result);

            enterprise_type = {
                "id": result[0].enterprise_type_code,
                "enterprise_type_name" : empresas_index[result[0].enterprise_type_code-1]
            }
            result[0].enterprise_type = enterprise_type;
            delete result[0].enterprise_type_code;
            res.send({ "enterprise": result[0], "success": true});
        })
    }
    else //Em caso de conexao com host externo
    {
        try{
            res.send(res.body);
            }catch(err){  res.status(500).send('Internal server error') }
    }
})


    //TESTE PARA ADICIONAR EMPRESA NO BANCO
    router.get('/addempresa', (req, res) => {
      //  let post = {title: 'Post one', body: 'this is post number one'};
        let exemplo =        
            {
                id: 5,
                email_enterprise: null,
                facebook: null,
                twitter: null,
                linkedin: null,
                phone: null,
                own_enterprise: false,
                enterprise_name: "Árbol Sabores",
                photo: null,
                description: "We are Arbol Sabores, a new generation of healthy food that has a positive impact in the environment and society. We want to change the world through the feeding behaviors of the society, giving seeds of urban orchards in ours products and by innovating with biodegradable packing.",
                city: "Santiago",
                country: "Chile",
                value: 0,
                share_price: 5000.0,
                enterprise_type_code: 11
            }

        let sql = 'INSERT INTO enterprise set ?'
        let query = db.query(sql, exemplo,(err, result) => {
            if (err) throw err;
            console.log(result);
            res.send('empresa adicionada');
        });
    
    });

const empresas_index = [
    "Agro",
    "Aviation",
    "Biotech",
    "Eco",
    "Ecommerce",
    "Education",
    "Fashion",
    "Fintech",
    "Food",
    "Games",
    "Health",
    "IOT",
    "Logistics",
    "Media",
    "Mining",
    "Products",
    "Real Estate",
    "Service",
    "Smart City",
    "Social",
    "Software",
    "Technology",
    "Tourism",
     "Transport",
]

module.exports = router;

