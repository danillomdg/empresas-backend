const router = require('express').Router();
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const {db} = require('../database');

//Teste e exemplo de registro para um usuário no banco MySQL
router.post('/registerteste', async (req, res) => {
    //Encripta a senha
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash('12341234', salt);

    //Cria o usuario
    const investorTeste = 
    {
        id: 1,
        investor_name: "Teste Apple",
        email: "testeapple@ioasys.com.br",
        hashed_pw: hashedPassword,
        city: "BH",
        country: "Brasil",
        balance: 0,
        photo: null,
        portfolio: '{"enterprises_number": 0,"enterprises": []}',
        portfolio_value: 1000000.0,
        first_access: false,
        super_angel: false
    }
    //Insere no banco de dados
    let sql = 'INSERT INTO investor set ?'
    let query = db.query(sql, investorTeste,(err, result) => {
        if (err) throw err;
        console.log(result);
        res.send('user registered')
    })
});


//ROTA DE LOGIN
router.post('/sign_in', async (req, res) => {
    //checa se o email existe
    let user = [];
    let sql = `SELECT * FROM investor WHERE email = '${req.body.email}'`
    
    //Busca as credenciais no banco de dados
    if (db.state != 'disconnected') 
    {
    let query =  db.query(sql, async (err, results) => {
        if (err) throw err;
        console.log(results);
        user = results;
        console.log(results.length);

        if (user.length == 0) return res.status(401).send('user zero');

        //checa a senha encriptada
        const validPass = await bcrypt.compare(req.body.password, results[0].hashed_pw);
        if(!validPass) return res.status(401).send('Invalid login credentials. Please try again. 2')

        //Cria o token JWT
        const token = jwt.sign({email : user[0].email}, process.env.TOKEN_SECRET);
        const client = jwt.sign({id: user[0].id}, process.env.CLIENT_SECRET);
        console.log('logado');
        res.set({
            'access-token': token,
            'client': client,
            'uid' : req.body.email,

        })
        //deleta a senha e Converte o atributo 'portfolio' para json
        delete user[0].hashed_pw;
        user[0].portfolio = JSON.parse(user[0].portfolio);
        res.set(res.getHeaders())
        res.send({  "investor:":  user[0] ,  
                    "enterprise": null,
                    "success": true });

        })
    }
    else //Em caso de conexao com host externo
    {
        try{
        res.set(res.getHeaders())
        res.send(res.body)
        }catch(err){
        res.status(500).send('Internal server error')
    }
    }
    
    
});


module.exports = router;