const jwt = require('jsonwebtoken');

module.exports = (req,res,next) => {
    const token = req.header('access-token');
    const uid = req.header('uid');
    const client = req.header('client');

    //Compara a presenca dos 3 custom headers
    if(!token || !uid || !client) return res.status(401).send('Authorized users only.');

    try{
        //Verifica os tokens
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        const verifiedClient = jwt.verify(client, process.env.CLIENT_SECRET);
        req.user = verified;
        req.client = verifiedClient;

        //Verifica a igualdade do uid com o payload do token
        if (verified.email != uid)res.status(400).send('Authorized users only.')
        console.log(verified.email)
        next();
    }catch(err){
        res.status(400).send('Authorized users only.')
    }
}