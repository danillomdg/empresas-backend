const express = require('express');
const app = express();
const dotenv = require('dotenv');
//Importa rotas
const authRota = require('./rotas/auth');
const enmpresasRota = require('./rotas/enterprise');

const {db} = require('./database');


dotenv.config();


const port = 3000
const host = 'localhost'
const apiVersion = '/v1'

//Conecta ao banco de dados
  db.connect((err)=> {
    if(err){
      console.log(err);
    }
    console.log("Status do database MySQL: "+db.state);
 })





//MiddleWares
app.use(express.json());

//ROTAS
app.use('/api'+apiVersion+'/users/auth', authRota)
app.use('/api'+apiVersion+'/enterprises', enmpresasRota)

app.listen(port,host, console.log('Conectado ao servidor'));